﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

/*
Written by Robert Lloyd
rl636018@gmail.com
*/

namespace ButtonMapSummaryCompiler
{
    public static class Compiler
    {
        private const string SourceFilePath = "ButtonMap.c";
        private const string OutputFilePath = "ButtonMap.txt";
        private const string ButtonMatrixHeader = "Notes:"; //"RawInput, ButtonState, and MapSelectButtons:";
        private const string MapStartHeader = "ButtonMap: ";

        private static readonly string[] MapEndHeaders = new string[]
        {
            "disable buttons which don't exist on current controller",
            "default ButtonMap to empty"
        };
        
        private static void Main(string[] args)
        {
            var blockComments = new List<List<string>>();
            var inLineComments = new List<string>();
            bool readingBlockComment = false;

            // read blockComments and inLineCommments from file at SourceFilePath
            using (var fileRead = new FileStream(SourceFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) using (var file = new StreamReader(fileRead))
            {
                while (true)
                {
                    var line = file.ReadLine();

                    if (line == null)
                        break;

                    if (line.Contains("/*"))
                    {
                        readingBlockComment = true;

                        blockComments.Add(new List<string>());
                    }                        
                    else if (line.Contains("*/"))
                        readingBlockComment = false;
                    else if (readingBlockComment)
                        blockComments[blockComments.Count - 1].Add(line);
                    else if (line.Contains("//"))
                        inLineComments.Add(line);
                }
            }
            
            // remove blockComments which are empty or don't contain ButtonMatrixHeader
            blockComments.RemoveAll(x => (x.Count == 0) || (ButtonMatrixHeader == null) || (x[0].Contains(ButtonMatrixHeader) == false));

            // remove inLineComments before MapStartHeader and after MapEndHeader
            {
                var readingMapLines = false;
                var mapLines = new List<string>();

                foreach (var line in inLineComments)
                {
                    if (line.Contains(MapStartHeader))
                        readingMapLines = true;
                    else if (MapEndHeaders.Where(x => line.Contains(x)).Any())
                        readingMapLines = false;
                    
                    if (readingMapLines)
                        mapLines.Add(line);
                }

                inLineComments = mapLines;
            }

            // format white space in blockComments and inLineCommments
            {
                for (var i = 0; i < blockComments.Count; i++)
                    for (var j = 0; j < blockComments[i].Count; j++)
                        blockComments[i][j] = blockComments[i][j].FormatWhiteSpace();

                for (var i = 0; i < inLineComments.Count; i++)
                    inLineComments[i] = inLineComments[i].FormatWhiteSpace();
            }

            // remove code from inLineComments and apply minimum spacing
            {
                var minimumSpacing = -1;
                
                for (var i = 0; i < inLineComments.Count; i++)
                {
                    var spacing = "";

                    for (var j = 0; j < inLineComments[i].Length; j++)
                    {
                        if (inLineComments[i][j] == ' ')
                            spacing += ' ';
                        else
                            break;
                    }

                    inLineComments[i] = spacing + inLineComments[i].Substring(inLineComments[i].IndexOf("//") + 2).TrimStart();

                    if ((minimumSpacing == -1) || (spacing.Length < minimumSpacing))
                        minimumSpacing = spacing.Length;
                }

                for (var i = 0; i < inLineComments.Count; i++)
                    inLineComments[i] = inLineComments[i].Substring(minimumSpacing);
            }

            // add spacing lines to inLineComments between each appearance of MapStartHeader
            {
                var mapLines = new List<string>();

                for (var i = 0; i < inLineComments.Count; i++)
                {
                    if ((i > 0) && inLineComments[i].Contains(MapStartHeader))
                    {
                        mapLines.Add("");
                        mapLines.Add("-");
                        mapLines.Add("");
                    }

                    mapLines.Add(inLineComments[i]);
                }

                inLineComments = mapLines;
            }

            // write blockComments and inLineCommments to file at OutputFilePath
            using (var file = new StreamWriter(OutputFilePath))
            {
                if (blockComments.Count > 0)
                    foreach (var blockComment in blockComments)
                    {
                        foreach (var line in blockComment)
                            file.WriteLine(line);

                        file.WriteLine();
                        file.WriteLine("-");
                        file.WriteLine();
                    }

                for (var i = 0; i < inLineComments.Count; i++)
                {
                    if (i == inLineComments.Count - 1)
                        file.Write(inLineComments[i]);
                    else
                        file.WriteLine(inLineComments[i]);
                }
            }
        }

        private static string FormatWhiteSpace(this string input)
        {
            input = input.TrimEnd();

            for (var i = 0; i < input.Length; i++)
            {
                if (input[i] == '\t')
                {
                    var spaceCount = 4 - (i % 4);

                    var spaces = "";

                    for (var j = 0; j < spaceCount; j++)
                        spaces += ' ';

                    input = input.Substring(0, i) + spaces + input.Substring(i + 1);
                    i += (spaceCount - 1);
                }
            }

            return input;
        }
    }
}
