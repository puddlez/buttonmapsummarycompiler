Button Map Summary Compiler

Written by Robert Lloyd
rl636018@gmail.com


This project is used to generate a simple and easily readable button map summary file, compiled from the ButtonMap.c file included with encoder projects such as MultiUsbControllerEncoder.  Simply place in the same directory as ButtonMap.c and run.  ButtonMap.txt will be generated.